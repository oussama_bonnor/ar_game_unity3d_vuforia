﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class animScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.touchCount == 1)
	    {
	        transform.Rotate(0,0,Input.touches[0].position.x * Time.deltaTime /10);
	    }
	}

    public void attack()
    {
        StartCoroutine(at());
    }

    IEnumerator at()
    {
        GetComponent<Animator>().SetBool("attack", true); 
        yield return new WaitForSeconds(1.5f);
        GetComponent<Animator>().SetBool("attack", false);
    }

    public void ninjaScene()
    {
        SceneManager.LoadScene(1);
    }
    public void makamScene()
    {
        SceneManager.LoadScene(0);
    }
}
