﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ballBehavior : MonoBehaviour
{
    private Vector3 start;
    private Vector3 end;

    public GameObject text;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.touchCount == 1)
	    {
	        if (Input.touches[0].phase == TouchPhase.Began)
	            start = GameObject.Find("ARCamera").transform.GetChild(0).GetComponent<Camera>().ScreenToWorldPoint(Input.touches[0].position);
	        if (Input.touches[0].phase == TouchPhase.Ended)
	        {
                GetComponent<Rigidbody>().useGravity = true;
	            end = GameObject.Find("ARCamera").transform.GetChild(0).GetComponent<Camera>().ScreenToWorldPoint(Input.touches[0].position);
	            text.GetComponent<Text>().text = ""+ (end - start);
                GetComponent<Rigidbody>().AddForce((start - end) * 10000);
	        }

	    }
    }
}
